# Notebooks GPUs

Notebooks to tests LCG stacks for GPUs

* Tensorflow example taken from https://github.com/tensorflow/docs/blob/r2.0rc/site/en/tutorials/estimators/cnn.ipynb
* PyTorch example taken from  https://github.com/pytorch/examples/tree/master/mnist
* scikit learn https://scikit-learn.org/stable/auto_examples/classification/plot_digits_classification.html