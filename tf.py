#!/usr/bin/env python
# coding: utf-8

# ##### Copyright 2018 The TensorFlow Authors.
# 
# 

# In[ ]:


#@title Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# In[ ]:


from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
import numpy as np
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession


# In[ ]:


config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

tf.logging.set_verbosity(tf.logging.INFO)
if tf.test.is_gpu_available(cuda_only=False):
    print('------ GPU ENABLED ------')


# In[ ]:


def cnn_model_fn(features, labels, mode):
  """Model function for CNN."""
  # Input Layer
  input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])

  # Convolutional Layer #1
  conv1 = tf.layers.conv2d(
      inputs=input_layer,
      filters=32,
      kernel_size=[5, 5],
      padding="same",
      activation=tf.nn.relu)

  # Pooling Layer #1
  pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

  # Convolutional Layer #2 and Pooling Layer #2
  conv2 = tf.layers.conv2d(
      inputs=pool1,
      filters=64,
      kernel_size=[5, 5],
      padding="same",
      activation=tf.nn.relu)
  pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

  # Dense Layer
  pool2_flat = tf.reshape(pool2, [-1, 7 * 7 * 64])
  dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)
  dropout = tf.layers.dropout(
      inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

  # Logits Layer
  logits = tf.layers.dense(inputs=dropout, units=10)

  predictions = {
      # Generate predictions (for PREDICT and EVAL mode)
      "classes": tf.argmax(input=logits, axis=1),
      # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
      # `logging_hook`.
      "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
  }

  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

  # Calculate Loss (for both TRAIN and EVAL modes)
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  # Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(
        loss=loss,
        global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

  # Add evaluation metrics (for EVAL mode)
  eval_metric_ops = {
      "accuracy": tf.metrics.accuracy(
          labels=labels, predictions=predictions["classes"])
  }
  return tf.estimator.EstimatorSpec(
      mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


# ### Load Training and Test Data
# 
# First, let's load our training and test data with the following code:

# In[ ]:


# Load training and eval data
((train_data, train_labels),
 (eval_data, eval_labels)) = tf.keras.datasets.mnist.load_data()

train_data = train_data/np.float32(255)
train_labels = train_labels.astype(np.int32)  # not required

eval_data = eval_data/np.float32(255)
eval_labels = eval_labels.astype(np.int32)  # not required


# We store the training feature data (the raw pixel values for 55,000 images of
# hand-drawn digits) and training labels (the corresponding value from 0–9 for
# each image) as [numpy
# arrays](https://docs.scipy.org/doc/numpy/reference/generated/numpy.array.html)
# in `train_data` and `train_labels`, respectively. Similarly, we store the
# evaluation feature data (10,000 images) and evaluation labels in `eval_data`
# and `eval_labels`, respectively.

# ### Create the Estimator {#create-the-estimator}
# 
# Next, let's create an `Estimator` (a TensorFlow class for performing high-level
# model training, evaluation, and inference) for our model. Add the following code
# to `main()`:

# In[ ]:


# Create the Estimator
mnist_classifier = tf.estimator.Estimator(
    model_fn=cnn_model_fn, model_dir="/tmp/mnist_convnet_model")


# The `model_fn` argument specifies the model function to use for training,
# evaluation, and prediction; we pass it the `cnn_model_fn` we created in
# ["Building the CNN MNIST Classifier."](#building-the-cnn-mnist-classifier) The
# `model_dir` argument specifies the directory where model data (checkpoints) will
# be saved (here, we specify the temp directory `/tmp/mnist_convnet_model`, but
# feel free to change to another directory of your choice).
# 
# Note: For an in-depth walkthrough of the TensorFlow `Estimator` API, see the tutorial [Creating Estimators in tf.estimator](../../guide/custom_estimators.md).

# ### Set Up a Logging Hook {#set_up_a_logging_hook}
# 
# Since CNNs can take a while to train, let's set up some logging so we can track
# progress during training. We can use TensorFlow's `tf.train.SessionRunHook` to create a
# `tf.train.LoggingTensorHook`
# that will log the probability values from the softmax layer of our CNN. Add the
# following to `main()`:

# In[ ]:


# Set up logging for predictions
tensors_to_log = {"probabilities": "softmax_tensor"}

logging_hook = tf.train.LoggingTensorHook(
    tensors=tensors_to_log, every_n_iter=50)


# ### Train the Model
# 
# Now we're ready to train our model, which we can do by creating `train_input_fn`
# and calling `train()` on `mnist_classifier`. In the `numpy_input_fn` call, we pass the training feature data and labels to
# `x` (as a dict) and `y`, respectively. We set a `batch_size` of `100` (which
# means that the model will train on minibatches of 100 examples at each step).
# `num_epochs=None` means that the model will train until the specified number of
# steps is reached. We also set `shuffle=True` to shuffle the training data. Then train the model a single step and log the output:

# In[ ]:


# Train the model
train_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": train_data},
    y=train_labels,
    batch_size=100,
    num_epochs=None,
    shuffle=True)

# train one step and display the probabilties
mnist_classifier.train(
    input_fn=train_input_fn,
    steps=1,
    hooks=[logging_hook])


# Now—without logging each step—set `steps=1000` to train the model longer, but in a reasonable time to run this example. Training CNNs is computationally intensive. To increase the accuracy of your model, increase the number of `steps` passed to `train()`, like 20,000 steps.

# In[ ]:


mnist_classifier.train(input_fn=train_input_fn, steps=1000)


# ### Evaluate the Model
# 
# Once training is complete, we want to evaluate our model to determine its
# accuracy on the MNIST test set. We call the `evaluate` method, which evaluates
# the metrics we specified in `eval_metric_ops` argument in the `model_fn`.
# Add the following to `main()`:

# In[ ]:


eval_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": eval_data},
    y=eval_labels,
    num_epochs=1,
    shuffle=False)

eval_results = mnist_classifier.evaluate(input_fn=eval_input_fn)
print(eval_results)

